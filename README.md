# Lower-level Find & Replace

An [ArchivesSpace ](http://archivesspace.org/) [plugin](https://github.com/archivesspace/tech-docs/blob/master/customization/plugins.md) to enhance the functionality of the Find and Replace job in ArchivesSpace. This plugin removes the restriction on editing controlled values in bulk, removes the ability to edit the `lang_materials` subrecord (to prevent confusion) and also allows running the job on a portion of a record tree, rather than the whole tree.
