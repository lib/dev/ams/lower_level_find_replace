ArchivesSpace::Application.routes.draw do
  scope AppConfig[:frontend_proxy_prefix] do
    match 'archival_objects/:id/models_in_graph' => 'archival_objects#models_in_graph', :via => [:get]
    match 'controlled_value_lists' => 'enumerations#read_only', :via => [:get]
  end
end
