ArchivesSpace::Application.extend_aspace_routes(File.join(File.dirname(__FILE__), "routes.rb"))

Rails.application.config.after_initialize do

  UtilsController.class_eval do
    def list_properties
      resource_type = params[:resource_type]

      list = []

      JSONModel(resource_type).schema['properties'].each do |name, defn|

        # We want to make dynamic enums like language editable, so we just comment out the following line.
        # next if params['editable'] && defn.has_key?('dynamic_enum')
        next if params['editable'] && %w(uri jsonmodel_type).include?(name)
        next if params['editable'] && defn['readonly']
        next if params['type'] && defn['type'] != params['type']
        
        # All the same, we want to make it clear that replacing enums works differently.
        enum_warning = defn.has_key?('dynamic_enum') ? " " + I18n.t("find_and_replace_job.enum_warning") : ""

        list << [name, I18n.t("#{resource_type}.#{name}") + enum_warning]
      end

      render :json => list
    end
  end

  ResourcesController.class_eval do
    set_access_control  "view_repository" => [:models_in_graph]

    def models_in_graph
      list_uri = JSONModel(:resource).uri_for(params[:id]) + "/models_in_graph"
      list = JSONModel::HTTP.get_json(list_uri)

      # lang_material has nothing useful in - remove it from the list to alleviate confusion
      if list && list.include?("lang_material")
        list.delete("lang_material")
      end

      render :json => (list || []).map {|type|
        [type, I18n.t("#{type == 'archival_object' ? 'resource_component' : type}._singular")]
      }
    end
  end

  ArchivalObjectsController.class_eval do
    set_access_control  "view_repository" => [:models_in_graph]
    
    def models_in_graph
      list_uri = JSONModel(:archival_object).uri_for(params[:id]) + "/models_in_graph"
      list = JSONModel::HTTP.get_json(list_uri)

      # lang_material has nothing useful in - remove it from the list to alleviate confusion
      if list && list.include?("lang_material")
        list.delete("lang_material")
      end

      render :json => (list || []).map {|type|
        [type, I18n.t("#{type == 'archival_object' ? 'resource_component' : type}._singular")]
      }
    end
  end

  JobsController.class_eval do
    def create
      @job_type = params['job']['job_type']

      job_data = params['job']

      # Knock out the _resolved parameter because it's often very large
      # and clean up the job data to match the schema types.
      job_data = ASUtils.recursive_reject_key(job_data) { |k| k === '_resolved' }
      
      # We've adjusted a couple of parameters in the find and replace job form to prevent duplicate JavaScript running.
      # Swap them back here, so the job runs as expected.
      job_data.keys.select {|key| key.start_with?("cam") }.each { |key| job_data[key.sub(/\Acam_/, "")] = job_data[key]; job_data.delete(key)}      
      
      job_data = cleanup_params_for_schema(job_data, JSONModel(@job_type.intern).schema)

      files = Hash[Array(params['files']).reject(&:blank?).map {|file|
                                    [file.original_filename, file.tempfile]}]

      job_params = ASUtils.recursive_reject_key(params['job']['job_params']) { |k| k === '_resolved' }

      job_data["repo_id"] ||= session[:repo_id]
      begin
        job = Job.new(@job_type, job_data, files,
                                    job_params
                     )
        uploaded = job.upload

        if (params['ajax'])
          if params[:iframePOST] # IE saviour. Render the form in a textarea for the AjaxPost plugin to pick out.
            render :plain => "<textarea data-type='json'>#{uploaded.to_json}</textarea>"
          else
            render :json => uploaded
          end
        else
          redirect_to :action => :show, :id => JSONModel(:job).id_for(uploaded['uri'])
        end

      rescue JSONModel::ValidationException => e
        @exceptions = e.invalid_object._exceptions
        @job = e.invalid_object
        @import_types = import_types
        @report_data = JSONModel::HTTP::get_json("/reports")

        params['job_type'] = @job_type

        render :new, :status => 500
      end
    end
  end

  EnumerationsController.class_eval do
  
    set_access_control "view_repository" => [:read_only]

    # this is identical to the out-of-the-box index method, except for the more liberal access control
    def read_only
      @enumerations = JSONModel(:enumeration).all
      @enumeration = JSONModel(:enumeration).find(params[:id]) if params[:id] and not params[:id].blank?
    end  
  end

end
